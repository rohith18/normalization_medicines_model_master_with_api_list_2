import re


def checking_regular_expressions_for_s(s):
    patterns = [r"\d+mg", r"\d+ml", r"\d+gm", r"\d+mcg", r"\d+%", r"\d+g", r"\d+'s", r"\d+`s"]
    replaced_s = s.lower()

    for item in patterns:
        pattern = re.compile(item)
        matches = pattern.finditer(replaced_s)
        for match in matches:
            text = replaced_s[match.span()[0]: match.span()[1]+1]
            replaced_s = replaced_s.replace(text, "")

    return replaced_s


def normalize_terms_for_s(s):

    try:
        replaced_s = (" ".join(s.split())).lower()

        replacable_strings = {"tablet": "tab", "tablets": "tab", "ointment": "oint", "capsule": "cap", "&": "and", ",": "", ".": "", "-": " "}
        for key, value in replacable_strings.items():
            replaced_s = replaced_s.replace(key, value)
            
        if (("(" and ")") in replaced_s):
            replaced_s = replaced_s.replace(replaced_s[ replaced_s.find("(") : replaced_s.find(")") + 1], "")

        replaced_str_list = replaced_s.split()
        updated_replaced_str_list = []
        for item in replaced_str_list:
            if item.isdecimal():
                pass
            else:
                updated_replaced_str_list.append(item)

        replaced_s = " ".join(updated_replaced_str_list)
        replaced_s = " ".join(replaced_s.split())
    except AttributeError:
        pass

    return replaced_s


def normalization(s):
    re_checked_s = checking_regular_expressions_for_s(s)
    normalized_s = normalize_terms_for_s(re_checked_s)

    return normalized_s
