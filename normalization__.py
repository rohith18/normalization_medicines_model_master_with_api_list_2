import copy
import json

import requests
from openpyxl import load_workbook
from fuzzywuzzy import fuzz
from test_normalization_s import normalization


def json_to_py(file_name):
    with open(file_name) as file:
        data = json.load(file)
    
    return data


def api_call(url, headers, data):
    response = requests.post(url, headers=headers, json=data)
    data = response.json()
    result = data.get("data").get("items")

    return result

model_item_master = json_to_py("model_item_master.json")

def append_to_list(list_name, item1, item2):
    if item1.get("name") in list_name.get("matched_master_medicine_names"):
        pass
    else:
        list_name.get("matched_master_medicine_names").append(item1.get("name"))
        list_name.get("matched_api_medicine_names").append(item2.get("name"))
        list_name.get("api_medicine_ids").append(item2.get("id"))
        list_name.get("model_item_master_mrp").append(item1.get("mrp"))
        list_name.get("api_mrp").append(item2.get("mrp"))
        list_name.get("model_item_master_manufacturer_names").append(item1.get("manufacturer"))
        list_name.get("api_manufacturer_names").append(item2.get("manufacturer"))


def create_custom_dict():
    return {"matched_master_medicine_names": [], "matched_api_medicine_names": [], "api_medicine_ids": [], "model_item_master_mrp": [], "api_mrp": [], "model_item_master_manufacturer_names": [], "api_manufacturer_names": []}

def match_medicine_names():
    url = "https://api.pharmacyone.io/prod/mobile_search_bill_item"
    headers = {"session-token": "wantednote", "Content-Type": "application/json"}

    list_percentage_100 = create_custom_dict()
    list_percentage_95 = create_custom_dict()
    list_percentage_90 = create_custom_dict()
    list_percentage_85 = create_custom_dict()
    remaining_list = create_custom_dict()

    called_medicine_names = []

    for item1 in model_item_master:
        # Maintaining called_medcine_names list to make sure we wont call the api for same medicine twice.
        if item1.get("name") in called_medicine_names:
            continue
        if item1.get("name"):
            data = {"search": item1.get("name")}
            api_call_list = api_call(url, headers, data)
            called_medicine_names.append(item1.get("name"))

        for item2 in api_call_list:
            percentage = fuzz.token_sort_ratio(normalization(item1.get("name")), normalization(item2.get("name")))
            if percentage == 100:
                append_to_list(list_percentage_100, item1, item2)
            elif percentage >= 95 and percentage < 100:
                append_to_list(list_percentage_95, item1, item2)
            elif percentage >= 90 and percentage < 95:
                append_to_list(list_percentage_90, item1, item2)
            elif percentage >= 85 and percentage < 90:
                append_to_list(list_percentage_85, item1, item2)
            else:
                append_to_list(remaining_list, item1, item2)

    return list_percentage_100, list_percentage_95, list_percentage_90, list_percentage_85, remaining_list


# Appending empty rows for consistent excel writes
def append_empty_rows(list_name, position):
    out_list = copy.deepcopy(list_name)
    out_list.insert(position, ["" for _ in range(len(list_name[0]))])

    return out_list


list_percentage_100, list_percentage_95, list_percentage_90, list_percentage_85, remaining_list = match_medicine_names()
list_100 = [value for value in list_percentage_100.values()]
list_95 = [value for value in list_percentage_95.values()]
list_90 = [value for value in list_percentage_90.values()]
list_85 = [value for value in list_percentage_85.values()]
remain_list = [value for value in remaining_list.values()]

list_95 = append_empty_rows(list_95, 2)
list_95 = append_empty_rows(list_95, 3)
list_95 = append_empty_rows(list_95, 4)

list_90 = append_empty_rows(list_90, 1)
list_90 = append_empty_rows(list_90, 3)
list_90 = append_empty_rows(list_90, 4)

list_85 = append_empty_rows(list_85, 1)
list_85 = append_empty_rows(list_85, 2)
list_85 = append_empty_rows(list_85, 4)

remain_list = append_empty_rows(remain_list, 1)
remain_list = append_empty_rows(remain_list, 2)
remain_list = append_empty_rows(remain_list, 3)

# Write to excel
def write_to_excel(workbook_name, worksheet_name, list_name):
    workbook = load_workbook(workbook_name)
    worksheet = workbook[worksheet_name]

    if worksheet_name == "matched_data":
        worksheet.append(["master_medicine_name", "api_medicine_name", "api_medicine_id", "model_item_master_mrp", "api_mrp", "model_item_master_manufacturer_name", "api_manufacturer_name"])
    elif worksheet_name == "unmatched_data":
        worksheet.append(["master_medicine_name", "api_medicine_name_95", "api_medicine_name_90", "api_medicine_name_85", "api_medicine_name_unmatched", "api_medicine_id", "model_item_master_mrp", "api_mrp", "model_item_master_manufacturer_name", "api_manufacturer_name"])
    else:
        pass

    for i in range(len(list_name[0])):
        appendable_row = []
        for list_ in list_name:
            appendable_row.append(list_[i])
        worksheet.append(appendable_row)

    workbook.save(workbook_name)


write_to_excel("normalized_data_using_api_2.xlsx", "matched_data", list_100)
write_to_excel("normalized_data_using_api_2.xlsx", "unmatched_data", list_95)
write_to_excel("normalized_data_using_api_2.xlsx", "unmatched_data", list_90)
write_to_excel("normalized_data_using_api_2.xlsx", "unmatched_data", list_85)
write_to_excel("normalized_data_using_api_2.xlsx", "unmatched_data", remain_list)

print(True)